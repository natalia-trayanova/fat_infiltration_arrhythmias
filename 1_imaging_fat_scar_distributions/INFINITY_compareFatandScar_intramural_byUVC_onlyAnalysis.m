% INFINITY_compareFatandScar_intramural_byUVC_onlyAnalysis.m
% Compares fat and scar distributions transmurally using the UVC coordinate
% system

%% Analyze the transmural distribution of substrate
load('INFINITY_substrate_transmural_UVC_2nd.mat')
all_transmuralCT_perc_2nd = zeros(length(CTdata_2nd),3); % [endo subendo mimyo subepi epi]
all_transmuralMRI_perc_2nd = all_transmuralCT_perc_2nd;
all_transmuralFAT_2nd = all_transmuralCT_perc_2nd;
all_transmuralScar_2nd = all_transmuralCT_perc_2nd;
for l = 1:length(CTdata_2nd)

    all_transmuralCT_perc_2nd(l,:) = CTdata_2nd{l}.perc_CT_2nd;
    all_transmuralMRI_perc_2nd(l,:) = MRIdata_2nd{l}.perc_MRI_2nd;
    all_transmuralFAT_2nd(l,:) = [CTdata_2nd{l}.endo_CT_2nd ... 
                                   CTdata_2nd{l}.midmyo_CT_2nd ... 
                                   CTdata_2nd{l}.epi_CT_2nd];
    all_transmuralScar_2nd(l,:) = [MRIdata_2nd{l}.endo_MRI_2nd... 
                                  MRIdata_2nd{l}.midmyo_MRI_2nd ... 
                                  MRIdata_2nd{l}.epi_MRI_2nd];
end

figure();
subplot(2,1,1); hold on;
for i = 1:3;histogram(all_transmuralMRI_perc_2nd(:,i));end
hold off
subplot(2,1,2); hold on;
for i = 1:3;histogram(all_transmuralCT_perc_2nd(:,i));end