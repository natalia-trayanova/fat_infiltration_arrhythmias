% INFINITY_VTcircuit_analysis_for_CT_and_MRI.m
% Analyzes the VT circuits in CT and MRI only models and looks at several
% characteristics

% Analyze VT circuit characteristics
% of CT and MRI models
load('analyze_VTannotations.mat') % Annotated version of the .xls file
load('extra_VTannotation_variables.mat')

% Load in Data files
load('INFINITY_CVstats_throughoutVTcircuit.mat')
load('INFINITY_tissueVolumes_throughoutVTcircuit.mat')
load('INFINITY_VTcircuit_dynamics.mat')
  
%% Look at the arrhythmogenicity between fat and scar models
% Corresponds to Figure 4
H = size(UVC_S,1);
C = size(UVC_S,2);

VTs_CT = sustainedVTcount(:,2);
VTs_MRI = sustainedVTcount(:,1);

VTs_base = zeros(size(UVC_S));
VTs_mid = zeros(size(UVC_S));
VTs_apex = zeros(size(UVC_S));

for h = 1:H
    for c = 1:C
        if isempty(UVC_S{h,c}); continue;end
        VTs_base(h,c) = [ sum(UVC_S{h,c}(:,1)>=2/3)];
        VTs_mid(h,c) = [sum( (UVC_S{h,c}(:,1)>1/3)&...
            (UVC_S{h,c}(:,1)<2/3) )];
        VTs_apex(h,c) = [sum(UVC_S{h,c}(:,1)<=1/3)];
    end
end

bar([sum(VTs_base,1);sum(VTs_mid,1);sum(VTs_apex,1)])

%% Look at the tissue distribution around each VT circuit
% And the intramural distribution
% Corresponds to Figure 5
H = size(sustainedVTindices_sorted,1);
C = size(sustainedVTindices_sorted,2);
normalCT_VTcircuits = [];
admixture_VTcircuits = [];
fat_VTcircuits = [];
fat_nonAct_VTcircuits = [];
normalMRI_VTcircuits = [];
gz_VTcircuits = [];
scar_VTcircuits = [];
scar_nonAct_VTcircuits = [];

epi_act_CT = []; endo_act_CT = []; midmyo_act_CT = [];
epi_act_MRI = []; endo_act_MRI = []; midmyo_act_MRI = [];

for h = 1:H
    for c = 1:C
        hc_idx = sustainedVTindices_sorted{h,c};
        if isempty(tissueVolumes{h,c}); continue; end
        tissue_volume_hc_names = fieldnames(tissueVolumes{h,c});
        for i = 1:length(hc_idx)
            current_VTname = [stim_name{hc_idx(i)} '_VT' num2str(VTlabels{hc_idx(i)})];
            tissueVol_i = tissueVolumes{h,c}.(current_VTname);
            VTcircuits_i = VTcircuits{h,c}.(current_VTname);
            if c == 2 % CT
                normalCT_VTcircuits = [normalCT_VTcircuits tissueVol_i.Voltotal(:,1)]; % Normal tissue in CT
                admixture_VTcircuits = [admixture_VTcircuits tissueVol_i.Voltotal(:,2)]; % Fat-myocardium admixture
                fat_VTcircuits = [fat_VTcircuits tissueVol_i.Voltotal(:,3)]; % inFAT
                fat_nonAct_VTcircuits = [fat_nonAct_VTcircuits ...
                    VTcircuits_i.total_act_in_nhood/VTcircuits_i.total_nodes_in_nhood]; % Non-activated nodes
                epi_act_CT = [epi_act_CT VTcircuits_i.epi_act]; % Epi acts
                midmyo_act_CT = [midmyo_act_CT VTcircuits_i.midmyo_act]; % Mid myo acts
                endo_act_CT = [endo_act_CT VTcircuits_i.endo_act]; % Endo acts
            elseif c == 1 % MRI
                normalMRI_VTcircuits = [normalMRI_VTcircuits tissueVol_i.Voltotal(:,1)]; % Normal tissue in MRI
                gz_VTcircuits = [gz_VTcircuits tissueVol_i.Voltotal(:,2)]; % Grey zone 
                scar_VTcircuits = [scar_VTcircuits tissueVol_i.Voltotal(:,3)]; % Scar
                scar_nonAct_VTcircuits = [scar_nonAct_VTcircuits ... % Non-activated in VT circuits
                    VTcircuits_i.total_act_in_nhood/VTcircuits_i.total_nodes_in_nhood]; 
                epi_act_MRI = [epi_act_MRI VTcircuits_i.epi_act]; % Epi activation
                midmyo_act_MRI = [midmyo_act_MRI VTcircuits_i.midmyo_act]; % Mid myocardial activation
                endo_act_MRI = [endo_act_MRI VTcircuits_i.endo_act]; % Endocardial activation
            end
        end
    end
end
total_MRItissue = (scar_VTcircuits+gz_VTcircuits+normalMRI_VTcircuits); % total tissue in MRI
total_CTtissue = (fat_VTcircuits+admixture_VTcircuits+normalCT_VTcircuits); % total tissue in CT
scarRemodeled = (scar_VTcircuits+gz_VTcircuits); % Scar as defined in paper
fatRemodeled = fat_VTcircuits+admixture_VTcircuits; % inFAT as defined in paper

% Examine the critical isthmus
allFat_isthmus = [fatRemodeled(5,:);...
                fatRemodeled(6,:)];
allScar_isthmus = [scarRemodeled(5,:);...
                scarRemodeled(6,:)];

perc_scarRemodeled = (scar_VTcircuits+gz_VTcircuits)./...
                    (scar_VTcircuits+gz_VTcircuits+normalMRI_VTcircuits); % As a percentage
perc_fatRemodeled = (fat_VTcircuits+admixture_VTcircuits)./...
                    (fat_VTcircuits+admixture_VTcircuits+normalCT_VTcircuits); % As a percentage
                
total_remodeled_perc_fat = (sum(fatRemodeled,1)./sum(total_CTtissue,1));
total_remodeled_perc_scar = sum(scarRemodeled,1)./sum(total_MRItissue,1);

epi_act_perc_MRI = epi_act_MRI./(epi_act_MRI+midmyo_act_MRI+endo_act_MRI); % Activation as a percentage
midmyo_act_perc_MRI = midmyo_act_MRI./(epi_act_MRI+midmyo_act_MRI+endo_act_MRI);
endo_act_perc_MRI = endo_act_MRI./(epi_act_MRI+midmyo_act_MRI+endo_act_MRI);
epi_act_perc_CT = epi_act_CT./(epi_act_CT+midmyo_act_CT+endo_act_CT);
midmyo_act_perc_CT = midmyo_act_CT./(epi_act_CT+midmyo_act_CT+endo_act_CT);
endo_act_perc_CT = endo_act_CT./(epi_act_CT+midmyo_act_CT+endo_act_CT);

figure()
bar([mean(endo_act_perc_MRI,2) mean(midmyo_act_perc_MRI,2) mean(epi_act_perc_MRI,2)],'stacked')
figure()
bar([mean(endo_act_perc_CT,2) mean(midmyo_act_perc_CT,2) mean(epi_act_perc_CT,2)],'stacked')
figure()
bar([mean(endo_act_perc_MRI,2) mean(midmyo_act_perc_MRI,2) mean(epi_act_perc_MRI,2) ...
    mean(endo_act_perc_CT,2) mean(midmyo_act_perc_CT,2) mean(epi_act_perc_CT,2)])
figure(); % Looking at the exit, outer loop, entrance
bar([mean(endo_act_perc_MRI(1:4,:),2) mean(midmyo_act_perc_MRI(1:4,:),2) mean(epi_act_perc_MRI(1:4,:),2) ...
    mean(endo_act_perc_CT(1:4,:),2) mean(midmyo_act_perc_CT(1:4,:),2) mean(epi_act_perc_CT(1:4,:),2)],'stacked')
figure(); % Looking at the exit, outer loop, entrance
bar([mean(endo_act_perc_MRI(5:8,:),2) mean(midmyo_act_perc_MRI(5:8,:),2) mean(epi_act_perc_MRI(5:8,:),2) ...
    mean(endo_act_perc_CT(5:8,:),2) mean(midmyo_act_perc_CT(5:8,:),2) mean(epi_act_perc_CT(5:8,:),2)],'stacked')

%% Look at total cycle length (ms)
H = size(sustainedVTindices_sorted,1);
C = size(sustainedVTindices_sorted,2);
TCL_CT = [];
TCL_MRI = [];
for h = 1:H
    for c = 1:C
        if c == 2
            TCL_CT = [TCL_CT;TCL(sustainedVTindices_sorted{h,c})];
        elseif c == 1
            TCL_MRI = [TCL_MRI;TCL(sustainedVTindices_sorted{h,c})];
        end
    end
end
           
%% First we'll take a look at the conduction velocities 
allCV_means = [];
allCV_stds = [];
for h = 1:size(CVstats,1)
    for c = 1:2
        if isempty(CVstats{h,c}); continue;end
        FN = fieldnames(CVstats{h,c});
        for f = 1:length(FN)
           allCV_means = [allCV_means CVstats{h,c}.(FN{f}).CVmeans]; 
           allCV_stds = [allCV_stds CVstats{h,c}.(FN{f}).CVstd]; 
        end
    end
end

% Make CV figure
figure()
bar([1:8],mean(allCV_means,2,'omitnan'));
ST = std(allCV_means,0,2);
hold on; er = errorbar([1:8],mean(allCV_means,2,'omitnan'),ST, ST);
er.LineStyle = 'none'; 
er.Color = [0 0 0];
hold off
xlabel('Activation Isochrone','fontsize',14)
ylabel('Average Conduction Velocity (cm/s)','fontsize',14)
title('Conduction Velocities across Different Activation Isochrones','fontsize',...
    16)

% Next we will compare between MRI and CT-based models
% They have VERY little difference, probably because of the same
% electrophysiological properties

allCV_means_MRI = [];
allCV_stds_MRI = [];
allCV_means_CT = [];
allCV_stds_CT = [];
for h = 1:size(CVstats,1)
    if isempty(CVstats{h,1}); continue;end
    FN = fieldnames(CVstats{h,1});
    for f = 1:length(FN)
       allCV_means_MRI = [allCV_means_MRI CVstats{h,1}.(FN{f}).CVmeans]; 
       allCV_stds_MRI = [allCV_stds_MRI CVstats{h,1}.(FN{f}).CVstd]; 
    end
end
for h = 1:size(CVstats,1)
    if isempty(CVstats{h,2}); continue;end
    FN = fieldnames(CVstats{h,2});
    for f = 1:length(FN)
        allCV_means_CT = [allCV_means_CT CVstats{h,2}.(FN{f}).CVmeans]; 
        allCV_stds_CT = [allCV_stds_CT CVstats{h,2}.(FN{f}).CVstd]; 
    end
end

figure()
bar([1:8],mean(allCV_means_CT,2,'omitnan'),'g');
ST = std(allCV_means_CT,0,2);
hold on; er = errorbar([1:8],mean(allCV_means_CT,2,'omitnan'),ST, ST);
er.LineStyle = 'none'; 
er.Color = [0 0 0];
hold off
xlabel('Activation Isochrone','fontsize',14)
ylabel('Average Conduction Velocity (cm/s)','fontsize',14)
title('CT Conduction Velocities across Activation Isochrones','fontsize',...
    16)

figure()
bar([1:8],mean(allCV_means_MRI,2,'omitnan'),'r');
ST = std(allCV_means_MRI,0,2);
hold on; er = errorbar([1:8],mean(allCV_means_MRI,2,'omitnan'),ST, ST);
er.LineStyle = 'none'; 
er.Color = [0 0 0];
hold off
xlabel('Activation Isochrone','fontsize',14)
ylabel('Average Conduction Velocity (cm/s)','fontsize',14)
title('MRI Conduction Velocities across Activation Isochrones','fontsize',...
    16)

exit_CT = [allCV_means_CT(1,:) ];
exit_MRI = [allCV_means_MRI(1,:)];
outerloop_CT = [allCV_means_CT(2,:) allCV_means_CT(3,:)];
outerloop_MRI = [allCV_means_MRI(2,:) allCV_means_MRI(3,:)];
entrance_CT = [allCV_means_CT(4,:)];
entrance_MRI = [allCV_means_MRI(4,:)];
commonpathway_CT = [allCV_means_CT(5,:) allCV_means_CT(6,:)...
                    allCV_means_CT(7,:) allCV_means_CT(8,:)];
commonpathway_MRI = [allCV_means_MRI(5,:) allCV_means_MRI(6,:)...
                     allCV_means_MRI(7,:) allCV_means_MRI(8,:)];
isthmus_CT = [allCV_means_CT(6,:) allCV_means_CT(7,:)];
isthmus_MRI = [allCV_means_MRI(6,:) allCV_means_MRI(7,:)];


%% Look at conduction velocity as a function of remodeled content
load('INFINITY_CV_vs_fatandscar.mat')
num_VTs = sum(sustainedVTcount,1);
CVs_MRI = zeros(8,num_VTs(1));
fat_MRIVTs = CVs_MRI;
scar_MRIVTs = CVs_MRI;
normal_MRIVTs = CVs_MRI;
CVs_CT = zeros(8,num_VTs(2));
fat_CTVTs = CVs_CT;
scar_CTVTs = CVs_CT;
normal_CTVTs = CVs_CT;
count_m = 1;
count_c = 1;
for p = 1: size(CV_by_inFATandscar,1)
    for c = 1:2
        VTs_inFATandscar = CV_by_inFATandscar{p,c};
        fnames = fieldnames(VTs_inFATandscar);
        for f = 1:length(fnames)
            struct_f = VTs_inFATandscar.(fnames{f});          
            if c == 1 % MRI models
                CVs_MRI(:,count_m) = struct_f.meanCV;
                fat_MRIVTs(:,count_m) = struct_f.fat_content;%./(struct_f.fat_content+struct_f.CT_normal);
                scar_MRIVTs(:,count_m) = struct_f.scar_content;%./(struct_f.scar_content+struct_f.MRI_normal);
                normal_MRIVTs(:,count_m) = struct_f.MRI_normal/1000*1.055; % Was NOT converted to g in original (still in mm^3)
                count_m = count_m+1;
            elseif c == 2 % CT models
                CVs_CT(:,count_c) = struct_f.meanCV;
                fat_CTVTs(:,count_c) = struct_f.fat_content;%./(struct_f.fat_content+struct_f.CT_normal);
                scar_CTVTs(:,count_c) = struct_f.scar_content;%./(struct_f.scar_content+struct_f.MRI_normal);
                normal_CTVTs(:,count_m) = struct_f.CT_normal/1000*1.055;  % Was NOT converted to g in original (still in mm^3)
                count_c = count_c+1;
            end
        end
    end
end
fat_MRIVTs(isnan(fat_MRIVTs)) = 0;
scar_MRIVTs(isnan(scar_MRIVTs)) = 0;
fat_CTVTs(isnan(fat_CTVTs)) = 0;
scar_CTVTs(isnan(scar_CTVTs)) = 0;

% CVs in different components of VT circuits.
CVs_ol_CT = (CVs_CT(2:3,:)')'; % CV in outer loop 
CVs_ol_MRI = (CVs_MRI(2:3,:)')';
CVs_cp_CT = (CVs_CT(5:8,:)')'; % CV in common pathway
CVs_cp_MRI = (CVs_MRI(5:8,:)')';
CVs_isth_CT = (CVs_CT(6:7,:)')'; % CV in critical isthmus
CVs_isth_MRI = (CVs_MRI(6:7,:)')';
CVs_ent_CT = (CVs_CT(4,:)')'; % CV in entrance
CVs_ent_MRI = (CVs_MRI(4,:)')';
CVs_ex_CT = (CVs_CT(1,:)')'; % CV in exit
CVs_ex_MRI = (CVs_MRI(1,:)')';

% Fat and scar in different components of VT circuits
ol_path_fat_MRI = sum(fat_MRIVTs(2:3,:),1)'; % Outer Loop
ol_path_scar_MRI = sum(scar_MRIVTs(2:3,:),1)';
ol_path_fat_CT = sum(fat_CTVTs(2:3,:),1)';
ol_path_scar_CT = sum(scar_CTVTs(2:3,:),1)';
ent_path_fat_MRI = (fat_MRIVTs(4,:)')'; ex_path_fat_MRI = (fat_MRIVTs(1,:)')'; % Entrance
ent_path_scar_MRI = (scar_MRIVTs(4,:)')'; ex_path_scar_MRI = (scar_MRIVTs(1,:)')';
ent_path_fat_CT = (fat_CTVTs(4,:)')'; ex_path_fat_CT = (fat_CTVTs(1,:)')';
ent_path_scar_CT = (scar_CTVTs(4,:)')'; ex_path_scar_CT = (scar_CTVTs(1,:)')';
cmn_path_fat_MRI = (fat_MRIVTs(5:8,:)')'; % Common pathway
cmn_path_scar_MRI = (scar_MRIVTs(5:8,:)')';
cmn_path_fat_CT = (fat_CTVTs(5:8,:)')';
cmn_path_scar_CT = (scar_CTVTs(5:8,:)')';
isthmus_path_fat_MRI = sum(fat_MRIVTs(6:7,:),1)';% Critical isthmus
isthmus_path_scar_MRI = sum(scar_MRIVTs(6:7,:),1)';
isthmus_path_fat_CT = sum(fat_CTVTs(6:7,:),1)';
isthmus_path_scar_CT = sum(scar_CTVTs(6:7,:),1)';

% correlations between fat and scar for and MRI moels
[rho p ] = corr(cmn_path_fat_CT(:),cmn_path_scar_CT(:))
[rho p ] = corr(cmn_path_fat_MRI(:),cmn_path_scar_MRI(:))
[rho p ] = corr(ol_path_fat_MRI(:),ol_path_scar_MRI(:))
[rho p ] = corr(ol_path_fat_CT(:),ol_path_scar_CT(:))
[rho p] = corr(ent_path_fat_MRI(:),ent_path_scar_MRI(:))
[rho p] = corr(ent_path_fat_CT(:),ent_path_scar_CT(:))
[rho p] = corr(ex_path_fat_MRI(:),ex_path_scar_MRI(:))
[rho p] = corr(ex_path_fat_CT(:),ex_path_scar_CT(:))
[rho p] = corr(isthmus_path_fat_CT(:),isthmus_path_scar_CT(:))
[rho p] = corr(isthmus_path_fat_MRI(:),isthmus_path_scar_MRI(:))

% Look at prevalence of inFAT + scar across VT circuits
F = isthmus_path_fat_MRI./...
    (isthmus_path_scar_MRI+ sum(normal_MRIVTs(6:7,:),1)'); % Use scar in denominator because it is MRI only model
S = isthmus_path_scar_MRI./...
    (isthmus_path_scar_MRI+ sum(normal_MRIVTs(6:7,:),1)');
disp('number of MRI VT isthmuses with inFAT')
sum(F>0.001) % Threshold as a percentage of tissue

% Plot unique CVs for VT circuits
figure()
bar([1:8],mean(CVs_CT,2,'omitnan'),'y');
ST = std(CVs_CT,0,2,'omitnan');
hold on; er = errorbar([1:8],mean(CVs_CT,2,'omitnan'),ST, ST);
er.LineStyle = 'none'; 
er.Color = [0 0 0];
hold off
xlabel('Activation Isochrone','fontsize',14)
ylabel('Average Conduction Velocity (cm/s)','fontsize',14)
title('CT Conduction Velocities across Activation Isochrones','fontsize',...
    16)

figure()
bar([1:8],mean(CVs_MRI,2,'omitnan'),'c');
ST = std(CVs_MRI,0,2,'omitnan');
hold on; er = errorbar([1:8],mean(CVs_MRI,2,'omitnan'),ST, ST);
er.LineStyle = 'none'; 
er.Color = [0 0 0];
hold off
xlabel('Activation Isochrone','fontsize',14)
ylabel('Average Conduction Velocity (cm/s)','fontsize',14)
title('MRI Conduction Velocities across Activation Isochrones','fontsize',...
    16)
