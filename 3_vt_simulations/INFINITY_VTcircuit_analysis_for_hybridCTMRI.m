% INFINITY_VTcircuit_analysis_for_hybridCTMRI.m
% Analyzes the VT circuits from hybrid CT-MRI models (looks at conduction
% velocity and the structural composition of the circuit)

load('extra_VTannotation_variables_hybridCTMRI.mat')
load('analyze_VTannotations_CTMRI.mat')
load('INFINITY_CVstats_throughoutVTcircuit_CTMRI.mat')
load('INFINITY_tissueVolumes_throughoutVTcircuit_CTMRI.mat')
load('INFINITY_VTcircuit_dynamics_CTMRI.mat')
       
%% Look at the arrhythmogenicity between fat and scar models
H = size(UVC_S,1);

VTdata = load('analyze_VTannotations.mat');

VTs_base_CTMRI = zeros(size(UVC_S));
VTs_mid_CTMRI = zeros(size(UVC_S));
VTs_apex_CTMRI = zeros(size(UVC_S));

VTs_base_CT = zeros(size(UVC_S));
VTs_mid_CT = zeros(size(UVC_S));
VTs_apex_CT = zeros(size(UVC_S));

VTs_base_MRI = zeros(size(UVC_S));
VTs_mid_MRI = zeros(size(UVC_S));
VTs_apex_MRI = zeros(size(UVC_S));

% Determine the number of basal, mid, and apical VTs
for h = 1:H
    if isempty(UVC_S{h}); continue;end
        VTs_base_CTMRI(h) = [ sum(UVC_S{h}(:,1)>=2/3)];
        VTs_mid_CTMRI(h) = [sum( (UVC_S{h}(:,1)>1/3)&...
            (UVC_S{h}(:,1)<2/3) )];
        VTs_apex_CTMRI(h) = [sum(UVC_S{h}(:,1)<=1/3)];
    for c = 1:2
        if c == 1
            VTs_base_MRI(h) = [ sum(VTdata.UVC_S{h,c}(:,1)>=2/3)];
            VTs_mid_MRI(h) = [sum( (VTdata.UVC_S{h,c}(:,1)>1/3)&...
                (VTdata.UVC_S{h,c}(:,1)<2/3) )];
            VTs_apex_MRI(h) = [sum(VTdata.UVC_S{h,c}(:,1)<=1/3)];
        elseif c == 2
            VTs_base_CT(h) = [ sum(VTdata.UVC_S{h,c}(:,1)>=2/3)];
            VTs_mid_CT(h) = [sum( (VTdata.UVC_S{h,c}(:,1)>1/3)&...
                (VTdata.UVC_S{h,c}(:,1)<2/3) )];
            VTs_apex_CT(h) = [sum(VTdata.UVC_S{h,c}(:,1)<=1/3)];
        end
    end
end

% Compare VTs across the different models
VTs_CTMRI = VTs_base_CTMRI+VTs_mid_CTMRI+VTs_apex_CTMRI;
VTs_CT = VTs_base_CT+VTs_mid_CT+VTs_apex_CT;
VTs_MRI = VTs_base_MRI+VTs_mid_MRI+VTs_apex_MRI;

bar([sum(VTs_base_CTMRI,1);sum(VTs_mid_CTMRI,1);sum(VTs_apex_CTMRI,1)])

figure();
ptags = {'','_base','_mid','_apex'};
ctags = {'CT','MRI','CTMRI'};
for j = 1:length(ptags)
    for c = 1:length(ctags)
        figure();
        histogram(eval(['VTs' ptags{j} '_' ctags{c}]))
        title(['VTs' ptags{j} '_' ctags{c}])
        xlim([-0.5 12.5]); ylim([0 14]);
        saveas(gcf,['/home/esung2/Documents/INFINITY/ManuscriptFigures/VTs' ...
            ptags{j} '_' ctags{c} '.svg'],'svg')
    end
end

%% Look at the tissue distribution around each VT circuit
% And the intramural distribution
H = size(sustainedVTindices_sorted,1);
normalCTMRI_VTcircuits = [];
admixture_gz_VTcircuits = [];
admixture_VTcircuits = [];
fat_VTcircuits = [];
fat_scar_nonAct_VTcircuits = [];
gz_VTcircuits = [];
fat_scar_VTcircuits = [];
scar_VTcircuits = [];
scar_nonAct_VTcircuits = [];

epi_act_CTMRI = []; endo_act_CTMRI = []; midmyo_act_CTMRI = [];

% Go through each model and each induced VT circuit and compute the amount
% of inFAT and scar in the circuit.
for h = 1:H
    hc_idx = sustainedVTindices_sorted{h};
    if isempty(tissueVolumes{h}); continue; end
    tissue_volume_hc_names = fieldnames(tissueVolumes{h});
    for i = 1:length(hc_idx)
        current_VTname = [stim_name{hc_idx(i)} '_VT' num2str(VTlabels{hc_idx(i)})];
        tissueVol_i = tissueVolumes{h}.(current_VTname); % Has information about tissue breakdown in each activation
        VTcircuits_i = VTcircuits{h}.(current_VTname); % Has the number of activated nodes
   
        normalCTMRI_VTcircuits = [normalCTMRI_VTcircuits tissueVol_i.Voltotal(:,1)]; % normal tissue
        admixture_gz_VTcircuits = [admixture_gz_VTcircuits tissueVol_i.Voltotal(:,2)]; % Fibro-fatty infiltration
        admixture_VTcircuits = [admixture_VTcircuits tissueVol_i.Voltotal(:,3)]; % Fat-myocardium admixture
        gz_VTcircuits = [gz_VTcircuits tissueVol_i.Voltotal(:,4)]; % grey zone
        fat_scar_VTcircuits = [fat_scar_VTcircuits tissueVol_i.Voltotal(:,5)]; % Both dense scar and inFAT
        fat_VTcircuits = [fat_VTcircuits tissueVol_i.Voltotal(:,6)]; % Dense inFAT
        scar_VTcircuits = [scar_VTcircuits tissueVol_i.Voltotal(:,7)]; % Dense scar

        fat_scar_nonAct_VTcircuits = [fat_scar_nonAct_VTcircuits ...
            1-VTcircuits_i.total_act_in_nhood/VTcircuits_i.total_nodes_in_nhood]; % Look at the non activated nodes in the circuit
        epi_act_CTMRI = [epi_act_CTMRI VTcircuits_i.epi_act]; % Epi activation
        midmyo_act_CTMRI = [midmyo_act_CTMRI VTcircuits_i.midmyo_act]; % Mid myocardial activation
        endo_act_CTMRI = [endo_act_CTMRI VTcircuits_i.endo_act]; % Endocardial activaiton
    end
end
total_CTMRItissue = normalCTMRI_VTcircuits+admixture_gz_VTcircuits+...
                    admixture_VTcircuits+gz_VTcircuits+fat_scar_VTcircuits+...
                    fat_VTcircuits+scar_VTcircuits; % Total amount of tissue in circuit
scarRemodeled = (scar_VTcircuits+gz_VTcircuits); % Our definition of scar in the paper
fatRemodeled = fat_VTcircuits+admixture_VTcircuits; % Our definition of inFAT in the paper
fatscarRemodeled = admixture_gz_VTcircuits+fat_scar_VTcircuits; %

%%% Stats on remodeling distribution throughout circuit
% Need to convert to grams (1.055 g/cm3)
FR = (sum(fatRemodeled,1)/1000*1.055)'; mean(FR)
FSR = (sum(fatscarRemodeled,1)/1000*1.055)'; mean(FSR)
SR = (sum(scarRemodeled,1)/1000*1.055)'; mean(SR)
std(FR)
std(FSR)
std(SR)

% Examine the critical isthmus
% Determine the number of VT critical isthmuses containing both fat and
% scar
allFat_isthmus = [fatscarRemodeled(5,:)+fatRemodeled(5,:);...
                fatscarRemodeled(6,:)+fatRemodeled(6,:)]./total_CTMRItissue(5:6,:);
allScar_isthmus = [fatscarRemodeled(5,:)+scarRemodeled(5,:);...
                fatscarRemodeled(6,:)+scarRemodeled(6,:)]./total_CTMRItissue(5:6,:);
thresh = 0.1; % Threshold for considering the presence of inFAT or scar
BothFS_isthmus = sum((sum(allFat_isthmus,1)>thresh)&(sum(allScar_isthmus,1)>thresh));
onlyS_isthmus = sum((sum(allFat_isthmus,1)<=thresh)&(sum(allScar_isthmus,1)>thresh));
onlyF_isthmus = sum((sum(allFat_isthmus,1)>thresh)&(sum(allScar_isthmus,1)<=thresh));


perc_scarRemodeled = (scar_VTcircuits+gz_VTcircuits)./...
                    total_CTMRItissue;
perc_fatRemodeled = (fat_VTcircuits+admixture_VTcircuits)./...
                    total_CTMRItissue;
perc_fatscarRemodeled = (fat_scar_VTcircuits+admixture_gz_VTcircuits)./total_CTMRItissue;
                
total_remodeled_perc_fat = (sum(fatRemodeled,1)./sum(total_CTMRItissue,1));
total_remodeled_perc_scar = sum(scarRemodeled,1)./sum(total_CTMRItissue,1);
total_remodeled_perc_fat_scar = sum(fatscarRemodeled,1)./sum(total_CTMRItissue,1);

epi_act_perc_CTMRI = epi_act_CTMRI./(epi_act_CTMRI+midmyo_act_CTMRI+endo_act_CTMRI);
midmyo_act_perc_CTMRI = midmyo_act_CTMRI./(epi_act_CTMRI+midmyo_act_CTMRI+endo_act_CTMRI);
endo_act_perc_CTMRI = endo_act_CTMRI./(epi_act_CTMRI+midmyo_act_CTMRI+endo_act_CTMRI);

figure()
bar([mean(endo_act_perc_CTMRI,2) mean(midmyo_act_perc_CTMRI,2) mean(epi_act_perc_CTMRI,2)],'stacked')
figure(); % Looking at the exit, outer loop, entrance
bar([mean(endo_act_perc_CTMRI(1:4,:),2) mean(midmyo_act_perc_CTMRI(1:4,:),2) mean(epi_act_perc_CTMRI(1:4,:),2)],'stacked')
figure(); % Looking at the exit, outer loop, entrance
bar([mean(endo_act_perc_CTMRI(5:8,:),2) mean(midmyo_act_perc_CTMRI(5:8,:),2) mean(epi_act_perc_CTMRI(5:8,:),2)],'stacked')

%% Look at total cycle length (ms)
H = size(sustainedVTindices_sorted,1);
TCL_CTMRI = [];
for h = 1:H
    TCL_CTMRI = [TCL_CTMRI;TCL(sustainedVTindices_sorted{h})];
end
           
%% First we'll take a look at the conduction velocities 
allCV_means = [];
allCV_stds = [];
for h = 1:size(CVstats,1)

    if isempty(CVstats{h}); continue;end
    FN = fieldnames(CVstats{h});
    for f = 1:length(FN)
       allCV_means = [allCV_means CVstats{h}.(FN{f}).CVmeans]; 
       allCV_stds = [allCV_stds CVstats{h}.(FN{f}).CVstd]; 
    end

end

% Make CV figure
figure()
bar([1:8],mean(allCV_means,2,'omitnan'));
ST = std(allCV_means,0,2,'omitnan');
hold on; er = errorbar([1:8],mean(allCV_means,2,'omitnan'),ST, ST);
er.LineStyle = 'none'; 
er.Color = [0 0 0];
hold off
xlabel('Activation Isochrone','fontsize',14)
ylabel('Average Conduction Velocity (cm/s)','fontsize',14)
title('Conduction Velocities across Different Activation Isochrones','fontsize',...
    16)

% Next we will compare between MRI and CT-based models
% They have VERY little difference, probably because of the same
% electrophysiological properties

allCV_means_MRI = [];
allCV_stds_MRI = [];
allCV_means_CT = [];
allCV_stds_CT = [];
for h = 1:size(CVstats,1)
    if isempty(CVstats{h,1}); continue;end
    FN = fieldnames(CVstats{h,1});
    for f = 1:length(FN)
       allCV_means_MRI = [allCV_means_MRI CVstats{h,1}.(FN{f}).CVmeans]; 
       allCV_stds_MRI = [allCV_stds_MRI CVstats{h,1}.(FN{f}).CVstd]; 
    end
end
for h = 1:size(CVstats,1)
    if isempty(CVstats{h,1}); continue;end
    FN = fieldnames(CVstats{h,1});
    for f = 1:length(FN)
        allCV_means_CT = [allCV_means_CT CVstats{h,1}.(FN{f}).CVmeans]; 
        allCV_stds_CT = [allCV_stds_CT CVstats{h,1}.(FN{f}).CVstd]; 
    end
end

figure()
bar([1:8],mean(allCV_means_CT,2,'omitnan'),'g');
ST = std(allCV_means_CT,0,2);
hold on; er = errorbar([1:8],mean(allCV_means_CT,2,'omitnan'),ST, ST);
er.LineStyle = 'none'; 
er.Color = [0 0 0];
hold off
xlabel('Activation Isochrone','fontsize',14)
ylabel('Average Conduction Velocity (cm/s)','fontsize',14)
title('CT Conduction Velocities across Activation Isochrones','fontsize',...
    16)

figure()
bar([1:8],mean(allCV_means_MRI,2,'omitnan'),'r');
ST = std(allCV_means_MRI,0,2);
hold on; er = errorbar([1:8],mean(allCV_means_MRI,2,'omitnan'),ST, ST);
er.LineStyle = 'none'; 
er.Color = [0 0 0];
hold off
xlabel('Activation Isochrone','fontsize',14)
ylabel('Average Conduction Velocity (cm/s)','fontsize',14)
title('MRI Conduction Velocities across Activation Isochrones','fontsize',...
    16)

exit_CTMRI = [allCV_means(1,:) ];
outerloop_CTMRI = [allCV_means(2,:) allCV_means(3,:)];
entrance_CTMRI = [allCV_means(4,:)];
commonpathway_CTMRI = [allCV_means(5,:) allCV_means(6,:)...
                    allCV_means(7,:) allCV_means(8,:)];
isthmus_CTMRI = [allCV_means(6,:) allCV_means(7,:)];


